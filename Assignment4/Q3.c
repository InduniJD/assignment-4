#include <stdio.h>

int main() {
    float a, b;

    printf("Enter a value for A: ");
    scanf("%f", &a);
    printf("Enter a value for B: ");
    scanf("%f", &b);

    a = a + b;
    b = a - b;
    a = a - b;

    printf("After swap A = %.2f and B = %.2f", a, b);

return 0;    
}