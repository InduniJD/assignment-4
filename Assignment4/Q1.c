#include <stdio.h>

int main() {
    int num1, num2, num3;
    int sum, average;


    printf("Enter three integer numbers: \n" , num1,num2, num3);
    scanf("%d %d %d", &num1, &num2, &num3);

    //calculate sum
    sum = num1 + num2 + num3;

    //calculate the average

    average = sum / 3;

    printf("Sum of the three numbers is: %d\n", sum);
    printf("Average of the three numbers: %d", average);

return 0;
}