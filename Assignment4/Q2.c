#include <stdio.h>

int main () {
    
    const float pi = 3.14;
    float h, r, volume;

    printf("Enter the height: ");
    scanf("%f", &h);

    printf("Enter the radius: ");
    scanf("%f", &r);

    volume = (pi * r * r * h) / 3;

    printf("Volume of the cone = %f", volume);

return 0;
}