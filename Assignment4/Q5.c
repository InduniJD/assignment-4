#include <stdio.h>

int main(){

    int A, B, Z;
    A = 10;
    B = 15;

    printf(" A & B = %d\n", A & B);
    
    printf("A ^ B = %d\n", A ^ B);

    printf("~A = %d\n", ~A);

    printf("A << 3 = %d\n", A << 3);

    printf("B >> 3 = %d\n", B >> 3);

return 0;    
}